package main

// Contain constraint of the billboard and treatment

import (
	"regexp"
	"errors"
	"fmt"
)

const LIMIT_MAX_SIZE = 1000
const LIMIT_MIN_SIZE = 1

const LIMIT_MAX_LENGTH_TEXT = 1000
const LIMIT_MIN_LENGTH_TEXT = 1

const LIMIT_MIN_TEST_CASE = 1
const LIMIT_MAX_TEST_CASE = 20

// 1 ≤ W, H ≤ 1000
func CheckLimitSize(width,height uint) error {
	if  width >= LIMIT_MIN_SIZE && width <= LIMIT_MAX_SIZE &&
		height >= LIMIT_MIN_SIZE && height <= LIMIT_MAX_SIZE {
			return nil
	}
	return errors.New(fmt.Sprintf( "billboard size must is between : %d ≤ W, H ≤ %d",LIMIT_MIN_SIZE,LIMIT_MAX_SIZE))
}

// The text will contain only lower-case letters a-z, upper-case letters A-Z, digits 0-9 and the space character.
func CheckRestrictedChar(text string) error {
	r := regexp.MustCompile("[a-zA-Z0-9]")
	if !r.MatchString(text) {
		return errors.New("the text will contain only lower-case letters a-z, upper-case letters A-Z, digits 0-9 and the space character")
	}
	return nil
}

// The text will not start or end with the space character, and will never contain two adjacent space characters.
func CheckBadSpace(text string) error {
	// Init error
	e := errors.New("the text will not start or end with the space character, and will never contain two adjacent space characters")

	// Check the beginning space
	if text[0] == ' ' {
		return e
	}

	// Check the end space
	if text[len(text)-1] == ' ' {
		return e
	}

	// Check the duplicate space
	c := 0
	for _,r := range text {
		if r == ' ' {
			c++
		} else {
			c = 0
		}
		if c > 1 {
			return e
		}
	}

	return nil
}

// The text in each case contains at least 1 character and at most 1000 characters.
func CheckTextMaxLength(text string) error {
	length := len(text)
	if length >= LIMIT_MIN_LENGTH_TEXT && length <= LIMIT_MAX_LENGTH_TEXT {
		return nil
	}
	return errors.New(fmt.Sprintf("the text in each case contains at least %d character and at most %d characters",LIMIT_MIN_LENGTH_TEXT,LIMIT_MAX_LENGTH_TEXT))
}


// Constraint on the number of test case.
func  CheckNumberTestCase(nbTestCase uint) error {
	if  nbTestCase >= LIMIT_MIN_TEST_CASE && nbTestCase <= LIMIT_MAX_TEST_CASE {
		return nil
	}
	return errors.New("the number of test case must be : 1 ≤ T ≤ 20")
}
