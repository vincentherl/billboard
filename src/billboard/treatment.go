package main

// Contain the treatment of the test case file.

import (
	"fmt"
	"os"
	"bufio"
	"strconv"
	"regexp"
	"errors"
)

// Input of program you must provide an path to test case file.
func main() {
	args := os.Args[1:]
	if len(args) < 1 {
		fmt.Println("please provide the input test case file path")
		os.Exit(1)
	}

	// Open output file
	inputFile,err := os.Open(args[0])
	if err != nil {panic(inputFile)}
	defer inputFile.Close()

	// Create scanner for read the file line by line.
	scan := bufio.NewScanner(inputFile)
	scan.Split(bufio.ScanLines)

	// get the number of test case
	scan.Scan()
	nbCase64,err := strconv.ParseUint(scan.Text(),10,32)
	nbCase := uint(nbCase64)
	// panic if the constraint of number of test case is not fulfill.
	err = CheckNumberTestCase(nbCase)
	if err != nil{panic(err)}

	// Create output empty billboard slice.
	listBillBoard := make([]*BillBoard,0)

	/***** INPUT TREATMENT *****/
	i := uint(0)
	for scan.Scan() && i < nbCase {

		fileLine := scan.Text()

		// Create billboard instance
		newBillBoard,err := NewBillBoardByTestCase(i+1,fileLine)
		if err != nil {
			fmt.Println(err)
		} else {
			// Put the billboard instance on the output list
			listBillBoard = append(listBillBoard,newBillBoard)
		}
		i++
	}

	/***** OUTPUT TREATMENT *****/
	for _,billboard := range listBillBoard {
		// Compute the max font size and print the result
		billboard.ComputeMaxFontSize()
		fmt.Println(billboard)
	}

}

// Parse a test case line for create a billboard instance, throw an error if something wrong.
func NewBillBoardByTestCase(caseNumber uint,textCase string) (*BillBoard,error) {
	r := regexp.MustCompile("^([0-9]+) ([0-9]+) (.*)")
	tokens := r.FindStringSubmatch(textCase)

	// Create a parse error
	e := errors.New("parse test case fail")

	// Check number of token part.
	if len(tokens) < 4 {return nil,e}

	// Cast and create width
	width,err := strconv.ParseUint(tokens[1],10,32)
	if err != nil {return nil,e}

	// Cast and create height
	height,err := strconv.ParseUint(tokens[2],10,32)
	if err != nil {return nil,e}

	// Create an return billboard
	return NewBillBoard(caseNumber,uint(width),uint(height),tokens[3])
}