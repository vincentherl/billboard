package main

import (
	"testing"
	"reflect"
)

// Provide input test case billboard
func ProvideInput() []*BillBoard {
	return []*BillBoard{
		{1,20,6,"hacker cup",0},
		{2,100,20,"hacker cup 2013",0},
		{3,10,20,"MUST BE ABLE TO HACK",0},
		{4,55,25,"Can you hack",0},
		{5,100,20,"Hack your way to the cup",0},
	}

}

// Provide output test case billboard
func ProvideOuput() []*BillBoard {
	input := ProvideInput()

	input[0].FontSize = 3
	input[1].FontSize = 10
	input[2].FontSize = 2
	input[3].FontSize = 8
	input[4].FontSize = 7

	return input
}

// Check if the simulation work with the output value.
func TestValidationSimulation(t *testing.T) {
	input := ProvideOuput()
	for _,bb := range input {
		lines,err := bb.Simulation()
		if err != nil {
			t.Error(err,lines)
		}
	}
}

// Check is the font size is set correctly and input match with expecting output.
func TestValidationComputeMaxFontSize(t *testing.T) {
	input := ProvideInput()
	expect := ProvideOuput()
	for i := 0; i < len(input); i++ {
		input[i].ComputeMaxFontSize()
		if !reflect.DeepEqual(input[i],expect[i]) {
			t.Error("Fail to test case : ",i)
		}
	}
}