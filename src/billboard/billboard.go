package main

// Contain the billboard structure, simulation, and compute of max font size.
// Simulation : it is a representation of the text split by line respecting the dimensions of the billboard.

import (
	"strings"
	"errors"
	"fmt"
)

const MAX_FONT_SIZE = 20

// Billboard description
type BillBoard struct {
	CaseNumber uint
	Width      uint
	Height     uint
	Text       string
	FontSize   uint
}

// Custom display of billboard
func (c BillBoard) String() string {
	return fmt.Sprintf("Case #%d: %d",c.CaseNumber,c.FontSize)
}

// Error display wrapper
type BillBoardError struct {
	CaseNumber uint
	RefError error
}
func (e BillBoardError) Error() string {
	return fmt.Sprintf("Case #%d [Error]: %s",e.CaseNumber,e.RefError)
}

// Create billboard and apply constraint
func NewBillBoard(casNumber,width,height uint,text string) (*BillBoard,error) {

	// Apply constraint and return an error if it's not respected
	err := CheckLimitSize(width,height)
	if err != nil {return nil,BillBoardError{casNumber,err}}
	err = CheckTextMaxLength(text)
	if err != nil {return nil,BillBoardError{casNumber,err}}
	err = CheckRestrictedChar(text)
	if err != nil {return nil,BillBoardError{casNumber,err}}
	err = CheckBadSpace(text)
	if err != nil {return nil,BillBoardError{casNumber,err}}

	billboard := BillBoard{
		CaseNumber:casNumber,
		Width:width,
		Height:height,
		Text:text,
	}

	return &billboard,nil
}

// Increment and set the max font size for the billboard
func (b *BillBoard) ComputeMaxFontSize() {
	for i := 1; i < MAX_FONT_SIZE; i++ {
		b.FontSize = uint(i)

		// Make a simulation, and detect the error
		_,err := b.Simulation()
		if err != nil {
			// roll-back last font size upgrade
			b.FontSize--
			break
		}
	}
}

// Make a display simulation of the billboard, throw an error if there is some disturb
// Return the list of lines
func (b *BillBoard) Simulation() ([][]string,error) {
	// Split the text by space
	words := strings.Split(b.Text," ")

	// List of lines empty will greet by all line present in the billboard
	lines := make([][]string,0)
	iLine := uint(0)

	for i := 0; i < len(words); i++ {

		// Check if height is overtaking by the lines.
		if !b.CheckLinesHeight(iLine+1) {
			return lines,BillBoardError{b.CaseNumber,errors.New("to many line, the height of billboard is overtake")}
		}

		// Provide a new line.
		nbLine := uint(len(lines))
		if nbLine <= iLine || nbLine == 0 {
			lines = append(lines,[]string{})
		}

		// Get the current line
		currentLine := lines[iLine]
		// Add new word to line
		currentLine,err := b.AddWordToLine(currentLine,words[i])

		// Exit function et throw an error if a word overtake the line or other strong problems.
		if err != nil {
			return lines,err
		}
		// Detect if the line is ended
		if currentLine == nil {
			// Roll back to the current word how has not added
			i--
			// Go to the next line
			iLine++
			continue
		}
		// Save the current line in the list
		lines[iLine] = currentLine
	}

	return lines,nil
}


// Add word to line, and return this one, return nil if the line overtaking his width.
// If one word overtaking the billboard width an error id throw
func (b BillBoard) AddWordToLine(line []string,word string) ([]string,error) {

	// Append word to line
	line = append(line,word)
	// Check if the line respect the width
	if b.CheckLineMaxWith(line) {
		return line,nil
	} else if len(line) == 1 {
		// throw an error if the first word overtaking a billboard width
		return nil,BillBoardError{b.CaseNumber,errors.New("a word ["+word+"] overtake the billboard width")}
	}
	return nil,nil
}

// Return true is total of lines respect the height of billboard otherwise return false.
func (b BillBoard) CheckLinesHeight(nbLine uint) bool {
	if (b.FontSize * nbLine) > b.Height {
		return false
	}
	return true
}

// Return true is the line respect the width of billboard otherwise return false.
func (b BillBoard) CheckLineMaxWith(line []string) bool {
	widthLine := b.WidthLine(line)
	if widthLine > b.Width {
		return false
	}
	return true
}

// Return the width of one line by words.
func (b BillBoard) WidthLine(line []string) (width uint) {

	// Add the width words
	for _,word := range line {
		width = width + b.WidthWord(word)
	}

	// Add the width of space between words
	width = width + (uint(len(line)) - 1) * b.FontSize
	return width
}

// Return the width of a word.
func (b BillBoard) WidthWord(word string) uint {
	return uint(len(word)) * b.FontSize
}