package main

import "testing"


func TestCheckNumberTestCase(t *testing.T) {
	if CheckNumberTestCase(LIMIT_MIN_TEST_CASE) != nil {
		t.Error("Correct number of test case fail")
	}
	if CheckNumberTestCase(LIMIT_MIN_TEST_CASE-1) == nil {
		t.Error("low overtaking of test case")
	}
	if CheckNumberTestCase(LIMIT_MAX_TEST_CASE+1) == nil {
		t.Error("overtaking of test case")
	}
}


func TestBillBoard_LimitSize(t *testing.T) {
	if CheckLimitSize(1,1) != nil {
		t.Error("Correct size fail")
	}
	if CheckLimitSize(LIMIT_MAX_SIZE+1,10) == nil {
		t.Error("Overtaking width")
	}
	if CheckLimitSize(10,LIMIT_MAX_SIZE+1) == nil {
		t.Error("Overtaking height")
	}
	if CheckLimitSize(LIMIT_MIN_SIZE-1,10) == nil {
		t.Error("Low Overtaking width")
	}
	if CheckLimitSize(10,LIMIT_MIN_SIZE-1) == nil {
		t.Error("Low Overtaking height")
	}
}

func TestBillBoard_RestrictedChar(t *testing.T)  {
	if CheckRestrictedChar("abh ss 99 AA ZZ") != nil {
		t.Error("Correct input text fail")
	}
	if CheckRestrictedChar("é") == nil {
		t.Error("Bad input text fail")
	}
}

func TestBillBoard_CheckBadSpace(t *testing.T) {
	if CheckBadSpace("da da sd") != nil {
		t.Error("Corret space fail")
	}
	if CheckBadSpace(" dadasd") == nil {
		t.Error("Bad begening space")
	}
	if CheckBadSpace("dadasd ") == nil {
		t.Error("Bad end space")
	}
	if CheckBadSpace("da  dasd") == nil {
		t.Error("Bad double space")
	}
}

func TestBillBoard_CheckTextMaxLength(t *testing.T) {

	if CheckTextMaxLength("qaoisfjd9") != nil {
		t.Error("Correct lenght text")
	}

	maxText := [LIMIT_MAX_LENGTH_TEXT+1]byte{}
	s := string(maxText[:])
	if CheckTextMaxLength(s) == nil {
		t.Error("Bad lenght text")
	}
}