package main

import (
	"testing"
	"reflect"
)

// Simple billboard test
func TestNewBillBoardByTestCase(t *testing.T) {
	input := "10 20 MUST BE ABLE TO HACK"
	expect := &BillBoard{3,10,20,"MUST BE ABLE TO HACK",0}
	ouput,_ := NewBillBoardByTestCase(3,input)
	if !reflect.DeepEqual(expect,ouput) {
		t.Error("Fail to read test case")
	}
}