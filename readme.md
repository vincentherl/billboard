# Billboard

Has been made with golang 1.9.1

[Rule](rule.md)


### Install
Add the project to env GOPATH.
```
go install billboard
```

### Run
```
./bin/billboard <input file>
```
example :  
```
./bin/billboard ./bin/input 

Case #1: 3
Case #2: 10
Case #3: 2
Case #4: 8
Case #5: 7
```

Example if a test case do not respect constrains (case 2 for size).
```
./bin/billboard ./bin/input_error_2

Case #2 [Error]: billboard size must is between : 1 ≤ W, H ≤ 1000
Case #1: 3
Case #3: 2
Case #4: 8
Case #5: 7
```

### Test
```
go test billboard -v
=== RUN   TestValidationSimulation
--- PASS: TestValidationSimulation (0.00s)
=== RUN   TestValidationComputeMaxFontSize
--- PASS: TestValidationComputeMaxFontSize (0.00s)
=== RUN   TestCheckNumberTestCase
--- PASS: TestCheckNumberTestCase (0.00s)
=== RUN   TestBillBoard_LimitSize
--- PASS: TestBillBoard_LimitSize (0.00s)
=== RUN   TestBillBoard_RestrictedChar
--- PASS: TestBillBoard_RestrictedChar (0.00s)
=== RUN   TestBillBoard_CheckBadSpace
--- PASS: TestBillBoard_CheckBadSpace (0.00s)
=== RUN   TestBillBoard_CheckTextMaxLength
--- PASS: TestBillBoard_CheckTextMaxLength (0.00s)
=== RUN   TestNewBillBoardByTestCase
--- PASS: TestNewBillBoardByTestCase (0.00s)
PASS
ok      billboard       0.007s
```